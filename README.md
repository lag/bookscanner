
Image postprocessing:
* https://www.flameeyes.com/projects/unpaper

Formats: 
* http://content-conversion.com/wp-content/uploads/2014/09/CCS-METS-ALTO-Info_basic_20140909.pdf
* https://www.brodrigues.co/blog/2019-01-13-newspapers_mets_alto/

Digitization workflow thingies:
* https://github.com/intranda/goobi
* 

Ocr tools: 
* https://en.wikipedia.org/wiki/OCRopus  https://github.com/tmbdev/ocropy
* https://github.com/tmbdev/clstm
* https://en.wikipedia.org/wiki/CuneiForm_(software)
* https://github.com/tmbdev/hocr-tools
* https://www.danvk.org/2015/01/09/extracting-text-from-an-image-using-ocropus.html
* https://github.com/mittagessen/kraken (fork of ocropus)  http://kraken.re/
* https://openphilology.github.io/nidaba/

ocr output correction (hocr):
* http://jimgarrison.org/moz-hocr-edit/
* https://github.com/kba/hocrjs
* https://github.com/not-implemented/hocr-proofreader
* https://github.com/ultrasaurus/hocr-javascript
* https://www.openhub.net/p/hocr-tools
* https://github.com/UB-Mannheim/ocr-fileformat
* https://github.com/athento/hocr-parser
* https://github.com/UB-Mannheim/ocr-gt-tools  (editing transcriptions, commenting, tagging)

ocr output correction (ALTO):
* https://github.com/altoxml/documentation/wiki/Software
* https://github.com/KBNLresearch/europeananp-ner
* https://gitlab.com/jochre/jochre-alto-editor
* https://github.com/impactcentre/alto-editor
* http://dbis-halvar.uibk.ac.at/dokuwiki/doku.php?id=main:structify  < this one seems nice, allows you to tag chapter, headings and stuff 
* https://sourceforge.net/p/bnlviewer/home/Home/   (vieuwer from mets/alto)

* http://www.europeana-newspapers.eu/public-materials/tools/

annotation:
* https://webanno.github.io/webanno/

IIIF:
* https://github.com/jbaiter/demetsiiify

https://github.com/intranda/goobi-viewer-core

https://www.digitisation.eu/tools-resources/tools-for-text-digitisation/

non-free:
* https://www.primaresearch.org/tools/Aletheia


annotation:
* https://github.com/chakki-works/doccano

ocr services:
* internet archive: https://forum.diybookscanner.org/viewtopic.php?f=22&t=907
* https://archive.org/help/derivatives.php
* https://help.archive.org/hc/en-us/articles/360001820212-How-to-upload-scanned-images-to-make-a-book


formats:
* https://tei-c.org/



figure out firmware version: https://chdk.fandom.com/wiki/FAQ#Q._How_can_I_get_the_original_firmware_version_number_of_my_camera.3F

https://stuff2233.club/padlife/p/firstthird/export/txt

We have a bookscanner with cameras which are too old to be controlled by a computer.
What we do is we make photos and then download them on the pc. There we process with a script, number the photos and then pass to scantailor and tesseract.

1. Making photos: 
    a. empty the machines memory
    a1. set the machine on "shoot mode"
    b. take pictures of the book pressing the button
    c. connect the USB cable to the compurter and download the photos.

2. Download photos:
    a. set the machines on "player mode"
    b. use the scripts:
		getallfiles.sh to download all the images
    c. import images i


------------------------------------------------rudimentary bookscanner operation manual:
    
 with cameras off turn the switch on camera mode(camera symbol)

turn on with power button

optional: set zoom-->push 'print' button to activate or disactivate the bookscenner script, adjust the focus and press 'print' again
			set same aperture value on both cameras
			
take pictures and check that both counter adds up
 to upload photo switch to play mode
 ----------
odd/even numbering script (it takes the folders with pictures (right & left) as input and rename the files with even and odd numbers-->the output folder "out" contains images in the right order)

#SCRIPTZ ( usage ./rename.sh ODD_FOLDER EVEN_FOLDER )
#!/bin/bash
#
mkdir out

num=1
for file in $1/*.JPG; do
        cp "$file" "out/$(printf "%04u" $num).JPG"
        let num=num+2
done

num=0
for file in $2/*.JPG; do
        cp "$file" "out/$(printf "%04u" $num).JPG"
        let num=num+2
done

-----scantailor

open the folder out in scantailor and set the page (both for left and right, check a few pages)

save the project and the images

--------tesseract script
for file in out/*.tif; do
	tesseract -l nld "$file" "$file" hocr
	convert $file $file.jpg
#make sure you select the right language package!here nld (dutch)
done
hocr-pdf out > finalfile.pdf

other wise create a pdf and use some other commercial option online or offline (abbyy finereader..)



#### building chdk

apply the pathch in chdk folder
 make SHELL='bash -x' PLATFORM=a590 PLATFORMSUB=101b OPT_USE_GCC_EABI=1 fir
