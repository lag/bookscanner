Diese Konfigurationsdatein wurden erstellt um eine einfache Handhabung von SDM 
zu gew�hrleisten.

Eigenschaften:
- alle SDM Anzeigen wurde so positioniert dass diese mit den Canon Anzeigen 
nicht �berlappen (soweit m�glich)
- linke/rechte Konfiguration mit korrekter Definition der Kameraposition
- linke Kamera auf 180� gedreht oder horizontal eingestellt.
- getrennte deutsche und englische Konfigurationsdateien
- Alle Einstellung so vorgenommen dass diese f�r die �blichen stereoskopischen
Aufnahmen geeignet sind


HINWEIS: diese Konfigurationen werden NICHT automatisch vom SDM Installtions-
programm verwendet und m�ssen wie untern beschrieben manuell umkopiert werden!


Installation:
In dieser ZIP Datei befinden sich mehrere Unterverzeichnisse die die 
entsprechenden Konfigurationsdateien enthalten z.B die englischen (*_eng) oder
deutschen (*_ger) Dateien. In diesen Verzeichnissen befinden sich weitere 
Verzeichnisse f�r spezielle Kameramodelle. Diese Kameramodelle haben eigene 
Konfigurationen weil diese Modelle zum Teil unterschiedlche Einstellungen 
brauchen. Der Hauptgrund daf�r ist dass diese Modelle alle eine unterschiedliche
Farbpalette benutzen. F�r alle anderen Modelle die hier nicht explizit 
aufgef�hrt sind gelten die Konfigurationen aus dem Verzeichnis 'standard'. 
Unterhalb dieser Ordnerstuktur befinden sich weitere Order in denen die linken 
und rechten Konfigurationsdateien enthalten sind.
Es sind ferner Verzeichnisse vorhanden die die Konfigurationsdatein f�r das SDM
Installationsprogramm enthalten.
Hinweis: die neuen Konfigurationsdatein f�r die Kameras die NICHT die Standard-
konfiguration verwenden haben auch andere Dateinamen z.B 'chdk.cfg5'. Bitte 
diese Dateien NICHT nach '*.cfg' umbenennen sonst funktionieren die mit der 
entsprechenden Kamera nicht mehr!


Installation auf bereits konfigurierten SD Karten:
kopiere die Datei CHDK.CFG (links bzw. rechts) in das Verzeichnis /CHDK auf die
entsprechende SD Karte - vorhandene Datei muss ersetzt werden.

Installation f�r SDM Installationsprogramm:
kopiere alle Datein aus dem Verzeichnis 'SDM_install' in das SDM Installations-
verzeichnis auf dem PC (oder falls ein Verzeichnis namen 'CFG' gibt bitte in 
dieses Verzeichnis kopieren). Vorhandene Datein ersetzen lassen, Anschlie�end 
die SDM Installation wie gewohnt durchf�hren.

Hinweis: es existieren zwei verschiedene Konfiguratione f�r die linke Kamera - 
eine f�r ein Kameragespann montiert auf einer Z-Schiene (f�r solch ein Gespann
ist die Konfigurationsdatei aus dem Verzeichnis 'L' zu verwenden) sowie eine 
weitere Konfiguration f�r ein Kameragaspann mit nebeneinander montierten Kameras
Hierf�r ist die Konfiguration aus dem Verzeichnis 'L_horizontal' zu verwenden.
F�r den Installationprozess bitte die Datei 'CHDK.cfg.L' durch 'CHDK.cfg.LH'
ersetzen ('*.L' l�schen und '*.LH' nach '*.L' umbenennen)

Es wird KEINE Konfiguration f�r vertikale Komeragespanne geliefert - um eine
korrekte Darstellung bei einem solchen Gespann zu haben m�ssen die Parameter
'Kamera Orientierung' und 'Kamera Rotation' entsprechend dem vorhandenen Gespann
angepasst werden. Diese Parameter findet man im SDM Men� unter 'Stereo'


Viel Spass
Werner Bloos
Kontakt: werner@wbloos.de
Kommerzielle SDM Produkte: www.digi-dat.de