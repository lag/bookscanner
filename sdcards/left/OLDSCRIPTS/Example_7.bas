@title Equal-step focus
set_zoom_to_step 2
set_focus_to 500
equal_step_focus_bracketing
number_of_images_to_capture_is 3
" Equal-step bracketing"
" Press switch"
wait_for_switch_press
start_continuous_sequence
wait_until_done
end_continuous_sequence
end