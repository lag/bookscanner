@title Aperture
@param a numerator
@default a 1
@param b denominator
@default b 1
sync_off
aperture a . b
shoot
sync_on
end
