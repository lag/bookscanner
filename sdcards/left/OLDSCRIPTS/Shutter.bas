@title Shutter speed
@param a numerator
@default a 1
@param b denominator
@default b 1
sync_off
shutter_speed a / b
shoot
sync_on
end
