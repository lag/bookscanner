These configuration files were made for an easy to run SDM configuration

Features:
- all OSDs are placed so they do not (in most cases) overlap the Canon OSDs
- left/right configuration with correct camera postion
- left camera orientation set to inverted or horizontal 
- German and English configuration files
- all settings preset for general stereoscopic photography


NOTE: these configuration files are NOT used by the SDM installer, so you need 
to copy them according to the installation instructions below.


How to install:

You will find several subfolders in this ZIP file containing English (*_eng)
and German (*_ger) configuration files. First there are subfolders which contain
special configurations for specific camera models. These configurations are 
needed because these cameras use different settings for some features - mainly 
the colors are different since these cameras use a different color palette. For
all other models which are not named there use the files located in the 
'standard' folder. Within these subfolders you will find other folders 
containing separate configuration files as well as folders containing the 
installation files for SDM setup utility.
Note: the new configuration files which are different to the standard ones also
use different naming e.g 'chdk.cfg5' - please do NOT rename these files to 
'*.cfg' otherwise they will not work.

Installation on ready configured SD cards:
copy the file CHDK.CFG (left or right one) to the folder /CHDK on your SD card
and replace existing file.

Installation for SDM installation program:
copy all files from appropiate folder 'SDM_install' to your SDM installation 
folder on the PC (or if there is a folder called CFG please copy these files to
this folder) - and replace existing files - then run SDM installer and setup 
your SD cards.

Note: there are two different configurations for the left camera - one for a
setup using a z-bar (for such a setup use the standard configuration file 
located in the folder 'L') and for a side-by-side camera setup where left camera
is NOT rotated by 180deg - in this case use the configuration file located in
the folder 'L_horizontal' 
For the installation process replace the file 'CHDK.cfg.L' by 'CHDK.cfg.LH'
(delete the '*.L' file then rename the '*.LH' to '*.L')

There are NO configurations supplied for a vertical camera setup. If using such
a setup you need to change orientation and rotation to meet your setup. You can
find these parameters in SDM's STEREO setup.


enjoy
Werner Bloos
contact: werner@wbloos.de
commercial SDM products: www.digi-dat.de