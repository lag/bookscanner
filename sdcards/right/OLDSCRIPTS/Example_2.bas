@title Focus bracketing
rem set camera to continuous-shooting mode
rem make sure camera is in mode for focus override
set_zoom_to 7
set_focus_to 1000
auto_focus_bracketing
" Autofocus bracketing"
" Press switch"
wait_for_switch_press
start_continuous_sequence
wait_until_done
end_continuous_sequence
end