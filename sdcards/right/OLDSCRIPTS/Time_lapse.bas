@title Time-lapse
@param a first delay minutes
@default a 0
@param b first delay seconds
@default b 10
@param c shoot interval minutes
@default c 0
@param d shoot interval seconds
@default d 5
@param e number of repeats
@default e 3
@param f endless mode N/Y    0/1
@default f 0
@param g single/cont/time/burst
@default g 0
@param h Tv exposures
@default h 3
@param i Focus exposures
@default i 3
@param j bracket Tv/foc/Both   0/2
@default j 0
@param k light/dark/alternate   0/1/2
@default k 2
@param l num of 1/3 EV steps
@default l 3
@param m equal/auto/digi  0-2
@default m 0
@param n focus-step
@default n 50
@param p digiscope focus ref
@default p 500
@param q blank screen N/Y 0/1
@default q 0
@param r shutdown N/Auto/USB 0/1/2
@default r 0
@param s save stack N/Y 0/1
@default s 0
@param t Sunrise mode
@default t 0
@param u TXT/CSV 0/1
@default u 0

time_lapse a, b, c, d, e, f, g, h, i, j, k, l, m, n, p, q, r, s, t, u
end