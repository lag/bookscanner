@title Spot meter
:test_md_loop

readYUV 

r = (a*4096 + c*5743 + 2048)/4096
if r < 0 then r = 0
if r > 255 then r = 255

g = (a*4096 - b*1411 - c*2925 + 2048)/4096
if g < 0 then g = 0
if g > 255 then g = 255

u = (a*4096 + b*7258 + 2048)/4096
if u < 0 then u = 0
if u > 255 then u = 255

line_1 "# YUV ", a," ",b," ", c
line_2 "! RGB ", r," ",g," ", u

goto "test_md_loop"

