@title No-motion test
@param a time still
@default a 5
@param c sensitivity
@default c 128
@param d timeout
@default d 10
sync_off
:md_loop
b=0
print "About to shoot"
shoot_when_no_movement_for a, b, c, d
print N," ", b
N = N + 1
sleep_for_seconds 5
goto "md_loop"
end
