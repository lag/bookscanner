--[[
@title USB Tester
 
@param m Mode
   @default m 0
   @values  m State Width Pulses Count Key
 
--]]
 
function printf(...)
    local tic = get_day_seconds()
    print (string.format("%02d:%02d:%02d %s", tic/3600, tic%3600/60, tic%60, ...))
end
 
mode = m
set_console_layout(1, 1, 44, 10)
 
print("USB remote test started")
if(mode == 1) then mstr="pulse width" 
elseif (mode ==2) then mstr="pulses" 
elseif (mode ==3) then mstr="pulse count" 
elseif (mode ==4) then mstr="key" 
else mstr="state" end
print("   test mode : "..mstr)
print("Press MENU to exit")
 
set_config_value(121,1) -- make sure USB remote is enabled
 
usb_state=-1
rkey=0
 
repeat
    now = get_day_seconds()
 
 -- check USB state ?
    if ( mode == 0 ) then
        new_state = get_usb_power(1)
        if ( new_state ~= usb_state ) then 
            printf("state="..new_state)
            usb_state=new_state
        end
    end
 
 -- check USB pulse width ?
    if ( mode == 1 ) then
        x=get_usb_power(0)
        if ( x > 0 ) then printf("width ="..(x*10).." mSec") end
    end
 
 -- check USB pulse train?
    if ( mode == 2 ) then
        x=get_usb_power(2)
        if (x ~= 0 ) then 
            if ( x > 0 ) then printf("mark ="..(x*10).." mSec")
            else printf("space="..(-1*x*10).." mSec") end
        end
    end
 
 -- check USB pulse count ?
    if ( mode == 3 ) then
        x=get_usb_power(3)
        if ( x > 0 ) then printf("count="..x) end
    end
 
    wait_click(100)
 
  -- read USB as a camera key ?
    if ( mode == 4 ) then
        if (is_key("remote")) then 
            if (rkey==0) then 
                printf("remote pressed")
                rkey=1
            end    
        else  
            if (rkey==1) then 
                printf("remote released")
                rkey=0
            end
        end
    end
 
until is_pressed("menu")
 
set_config_value(121,0) -- make sure USB remote is disabled
