#!/bin/bash -e

. /home/user/src/bookscanner/scripts/common.sh

bookchooser

scantailorconf="$bookdir/data/$bookname.ScanTailor"

if [[ -f $scantailorconf ]]
then
	echo "scantailor config exists"
else



scantailor-cli \
	-v \
	--start-filter=1 \
	--end-filter=6 \
	--orientation=left \
	--layout=1.5 \
	--deskew=auto \
	--content-detection=normal \
	--color-mode=color_grayscale \
	--margins-left=1 \
	--margins-right=1 \
	--margins-top=2 \
	--margins-bottom=2 \
	--alignment-vertical=center \
	--alignment-horizontal=center \
	--output-dpi=300  \
	-o=$bookdir/data/$bookname.ScanTailor \
	$rawdir/ \
  $donedir/	
  fi	
	
	scantailor $scantailorconf

