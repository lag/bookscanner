#!/bin/bash -e

. /home/user/src/bookscanner/scripts/common.sh

detect_cameras
bookchooser

tmpdir=`mktemp -d`

mkdir $tmpdir/raw
echo "Fetching files from right"

mkdir $tmpdir/right
cd $tmpdir/right

gphoto2 --port "${camrightusb}" --camera "Canon PowerShot A590 IS" -P

num=0
for file in *.JPG; do 
  #cp "$file" "out/$(printf "%04u" $num).JPG" 
  mv "$file" "$tmpdir/raw/$(printf "%03u" $num).jpg" 

  let num=num+2 
done



echo "Fetching files from left"
mkdir $tmpdir/left
cd $tmpdir/left

gphoto2 --port "${camleftusb}" --camera "Canon PowerShot A590 IS" -P

num=1
for file in *.JPG; do
  #   cp "$file" "out/$(printf "%04u" $num).JPG"
  mv "$file" "$tmpdir/raw/$(printf "%03u" $num).jpg"

  let num=num+2 

done


#ls -lhtr $tmpdir/raw

mv $tmpdir/raw/*.jpg $rawdir/ 

checkdirs
