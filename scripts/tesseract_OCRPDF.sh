#!/bin/bash -e
#

# this is what runs with the desktop clicky thing
# needs hocr-tools and texcleaner script

. /home/user/src/bookscanner/scripts/common.sh

bookchooser

TMP=`mktemp -d`
TMP2=`mktemp -d`

for file in $donedir/*.tif; do
  echo $file
  bn=`basename $file`
  convert $file $TMP2/$bn.jpg
  /home/user/src/bookscanner/scripts/textcleaner.sh -g -e none -f 10 -o 5 $file $TMP/$bn.ocr.jpg
  tesseract -l nld "$TMP/$bn.ocr.jpg" "$TMP2/$bn" hocr

done

# make pdf sandwich out of color images + ocr text
hocr-pdf $TMP2 > $outdir/$bookname.pdf

echo "1: $TMP"
echo "2: $TMP2"
#rm -r $TMP
#rm -r $TMP2

echo "done!"



