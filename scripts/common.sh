

set -e # exit if any command has non-zero exit status (works in sh too)
set -u # fail on attempts to expand undefined enviroment variables
set -o pipefail # prevents errors in a pipeline from being masked

# Better error message than "set -e" produces (but only bash supports ERR)
#trap "echo int term; read; exit 23" SIGINT SIGTERM

function trapfunc () {

  echo "$0 exited with code: $?"
  echo "Press a key to exit"
  read

}

trap trapfunc ERR
trap trapfunc EXIT


#TMPDIR=/home/usr/tmp

bookname="no_book_specified"
bookscanningdir=/home/user/pusciicloud/bookscanning/books


# right camera scans even pages
camrightserial="CAF42ADD3554128ADF703512FC49BEC"
camrightfound=0

# left camera scans odd pages
camleftserial="BB8DE7484F64808AB54EAE3F2F213AE"
camleftfound=0

detect_cameras () {
  echo "camera's connected: "
  while read camline
  do 

    cammodel=$(echo $camline | cut -d ':' -f 1)
    usb=$(echo $camline | cut -d ':' -f 2)
    usbfirst=$(echo $usb | cut -d ',' -f 1)
    usbsecond=$(echo $usb | cut -d ',' -f 2)
    camserial=`lsusb -D /dev/bus/usb/$usbfirst/$usbsecond 2>/dev/null | grep iSerial | sed "s/[[:space:]].//g" | cut -d 'l' -f 2`
    if [ $camserial == $camrightserial ] 
    then
      camusage="right"
      camrightusb="usb:$usbfirst,$usbsecond"
      camrightfound=1
    elif	[ $camserial == $camleftserial ] 
    then
      camusage="left"
      camleftusb="usb:$usbfirst,$usbsecond"
      camleftfound=1

    else
      echo "camera not used in bookscanner"
      fi


      echo "* $camusage   :  $usb   :   $camserial   : $cammodel"
    done < <(gphoto2 --auto-detect | grep PowerShot)

    if [[ $camrightfound -eq 1 && $camleftfound -eq 1 ]]
    then
      echo both cams found
    else
      echo "a camera is missing, check connections"
      exit 3
      fi


    }



  function tofilename () {

  #echo "${1//[^[:alnum:]_-]/}"
  echo "$1" | sed -e 's/[^A-Za-z0-9._-]/_/g'
}

function setdirs () {

  if [[ ! -d $bookdir ]]; then

    echo "bookdir ( $bookdir ) is not a direcory "
    exit 2
  fi

  cd "$bookdir" && pwd
  # rotated jpeg, tiff, hocr
  donedir="$bookdir/data/done"

  # pdf's for publish etc
  outdir="$bookdir/data/out"

  # images from camera
  rawdir="$bookdir/data/raw"

}

function checkdirs () {

  if [[ ! -d $bookdir ]]; then

    echo "bookdir ( $bookdir ) is not a direcory "
    exit 2
  fi
  if [[ ! -d $donedir ]]; then

    echo "dir ( $donedir ) is not a direcory "
    exit 2
  fi
  if [[ ! -d $outdir ]]; then

    echo "dir ( $outdir ) is not a direcory "
    exit 2
  fi
  if [[ ! -d $rawdir ]]; then

    echo "dir ( $rawdir ) is not a direcory "
    exit 2
  fi





  echo ":: $bookname state"
  echo "   $(ls -1 $rawdir | wc -l) pictures from camera"
  echo "   $(ls -1 $outdir | wc -l) files in outdir"
  echo "   $(ls -1 $donedir | wc -l) files in donedir"


}

function newbook () {

  echo  "Name of new book (used in filenames):" ; read bookdirtyname
  #echo what whas typed: $bookdirtyname
  bookname="$(echo "$bookdirtyname" | sed -e 's/[^A-Za-z0-9._-]/_/g')"

  echo  "Catalog number (to look up metadata):" ; read bookdirtynumber
  #echo what whas typed: $bookdirtyname
  bookid="$(echo "$bookdirtynumber" | sed -e 's/[^A-Za-z0-9._-]/_/g')"



  #tofilename "dit is een testje ! ____- jee"
  #bookname="$(tofilename $bookdirtyname)"
  echo "name of book is: $bookname"
  bookdir="$bookscanningdir/$bookname"

  if [[ -d $bookdir ]]; then
    echo "bookdir already exists, choose a different name for your book"
    exit 2
  else
    mkdir "$bookscanningdir/$bookname"
    setdirs
    mkdir "$bookscanningdir/$bookname/data"
    mkdir $donedir
    mkdir $outdir
    mkdir $rawdir
    checkdirs
    generate_meta_file
  fi



}

function bookchooser () {

  echo "New or existing book?"
  select what in new existing
  do
    case $what in
      "new")
        newbook
        break
        ;;
      "existing")
        echo "Choose book :"

        select d in $bookscanningdir/*/ 
        do 
          test -n "$d" && break; 
          echo ">>> Invalid Selection" 
        done
        #	echo $d
        bookdir="$d"
        bookname=$(basename $bookdir)
        echo "selected book: $bookname"
        break
        ;;
    esac
  done


  setdirs
  checkdirs

}

function generate_meta_file ()
{
  cat > $bookdir/info.txt << EOF

Bagging-Date: `date --iso-8601`
Bag-Software-Agent: Lag bookscanner <https://laglab.org>
Spreads-Slug: $bookname
Lag-Bookname: $bookname
appelscha-id: $bookid
EOF
}
