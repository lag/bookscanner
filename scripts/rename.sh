#!/bin/bash

mkdir out

num=1 
for file in $1/*.JPG; do 
    cp "$file" "out/$(printf "%04u" $num).JPG" 
    let num=num+2 
done

num=0
for file in $2/*.JPG; do
    cp "$file" "out/$(printf "%04u" $num).JPG"
    let num=num+2 

done

## usage ./rename.sh ODD_FOLDER EVEN_FOLDER