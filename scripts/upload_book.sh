#!/bin/bash -e

. /home/user/src/bookscanner/scripts/common.sh

bookchooser


echo dir: $bookdir
echo name: $bookname

#dav:/remote.php/dav/files/lagbookscanner/bookscanning/books/

#webdavdir="https://cloud.puscii.nl/remote.php/dav/files/lagbookscanner/bookscanning/books/"

#cadaver $webdavdir <<EOF
#mput $bookdir
#EOF


#rclone ls pusciicloud_books:/

rclone copy $bookdir pusciicloud_books:/$bookname/

#nextcloudcmd -n $bookdir https://cloud.puscii.nl/remote.php/webdav/bookscanning/books/$bookname
